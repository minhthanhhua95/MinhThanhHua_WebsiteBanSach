﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoAnCoSo.Models;

namespace DoAnCoSo.Controllers
{
    public class NhaXuatBanController : Controller
    {
        // GET: NhaXuatBan
        QuanLyBanSachEntities db = new QuanLyBanSachEntities();
        public PartialViewResult NXBPartial()
        {
            return PartialView(db.NhaXuatBans.Take(5).ToList());
        }
        //hien thi sach theo nha xuat ban
        public ViewResult SachTheoNXB(int MaNXB=0)
        {
            //Kiểm tra NXB tồn tại hay không
            NhaXuatBan nxb = db.NhaXuatBans.SingleOrDefault(n => n.MaNXB == MaNXB);
            if (nxb == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            //Truy xuất danh sách các quyển sách theo NXB
            List<Sach> lstSach = db.Saches.Where(n => n.MaNXB == MaNXB).OrderBy(n => n.GiaBan).ToList();
            if (lstSach.Count == 0)
            {
                ViewBag.Sach = "Không có sách nào thuộc nhà xuất bản này";
            }
            //Gán danh sách NXB
            ViewBag.listNXB = db.NhaXuatBans.ToList();
            return View(lstSach);
        }
        public ViewResult DanhMucNXB()
        {
            //Lấy ra chủ đề đầu tiên trong csdl
            int MaNXB = int.Parse(db.NhaXuatBans.ToList().ElementAt(0).MaNXB.ToString());
            //Tạo 1 viewbag gán sách theo Nhà xuất bản đầu tiên trong csdl
            ViewBag.SachTheoNXB = db.Saches.Where(n => n.MaNXB == MaNXB).ToList();
            return View(db.NhaXuatBans.ToList());

        }
    }
}
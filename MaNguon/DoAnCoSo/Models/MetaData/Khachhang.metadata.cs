﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo.Models
{
    [MetadataTypeAttribute(typeof(KhachHangMetadata))]
    public partial class KhachHang
	{
        internal sealed class KhachHangMetadata
        {
            [Display(Name = "Họ và Tên")]
            public string HoTen { get; set; }
            [Display(Name = "Ngày Sinh")]
            public Nullable<System.DateTime> NgaySinh { get; set; }
            [Display(Name = "Giới Tính")]
            public string GioiTinh { get; set; }
            [Display(Name = "Điện Thoại")]
            public string DienThoai { get; set; }
            [Display(Name = "Tài Khoản")]
            public string TaiKhoan { get; set; }
            [Display(Name = "Mật Khẩu")]
            public string MatKhau { get; set; }
            
            public string Email { get; set; }
            [Display(Name = "Địa Chỉ")]
            public string DiaChi { get; set; }
        }
	}
}